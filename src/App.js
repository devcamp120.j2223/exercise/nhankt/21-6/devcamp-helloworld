import {gDevcampReact,percent} from "./info"

const h1= <h1>{gDevcampReact.title}</h1>
const img= <img src={gDevcampReact.image} width={500} alt="test"></img>


const pPercent = <p>{percent(gDevcampReact.studyingStudents,gDevcampReact.totalStudents)}%</p>
let element = "";
if (percent(gDevcampReact.studyingStudents,gDevcampReact.totalStudents)> 15){
  element = "Sinh viên đăng ký học nhiều"
}
else {element = "Sinh viên đăng ký học ít"} 
console.log(element)
const list = <ul>
        {
          gDevcampReact.benefits.map((element, index) => {
            return <li key={index}>{element}</li>
          })
        }
</ul>

function App() {
  return (
    <div style={{margin:"4rem 3rem"}}>
      <div>{h1}</div>
      <div>{img}</div>
      <div>{pPercent}</div>
      <h1>{element}</h1>
      <div>{list}</div>
    </div>
  );
}

export default App;
