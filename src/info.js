const gDevcampReact = {
    title: 'Chào mừng đến với Devcamp React',
    image: 'https://reactjs.org/logo-og.png',
    benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
    studyingStudents: 20,
    totalStudents: 100
  }

  const percent = (stydying,total)=>{
    return parseInt((stydying/total) *100)
  }

  export {gDevcampReact,percent}